@smoke-test @api @selenium
Feature: Selenium is available to run javascript.
  In order to test javascript functionality
  As an Developer
  I need to be able to run tests through Selenium.

  Scenario: Confirm that the front page works without javascript.
    Given I am not logged in
     When I am on the homepage
     Then I should see "Log in"

  @javascript
  Scenario: Confirm that the front page works with javascript (via Selenium).
    Given I am not logged in
     When I am on the homepage
     Then I should see "Log in"

  Scenario: Confirm that we can login without javascript.
    Given I am not logged in
     When I am on "/user/1"
     Then I should see "Access denied"
      And I should not see "Member for"
     When I am logged in as a "Administrator"
      And I am on "/user/1"
     Then I should see "Member for"
      And I should not see "Access denied" in the "content" region

  @javascript
  Scenario: Confirm that we can login with javascript (via Selenium).
    Given I am not logged in
     When I am on "/user/1"
     Then I should see "Access denied"
      And I should not see "Member for"
     When I am logged in as a "Administrator"
      And I am on "/user/1"
     Then I should see "Member for"
      And I should not see "Access denied"

  Scenario: Confirm that we can login manually works without javascript.
    Given I am not logged in
     When I am on "/user/1"
     Then I should see "Access denied"
      And I should not see "Member for"
    Given I am on "/user/login"
     When I enter "dev" for "Username"
      And I enter "pwd" for "Password"
      And I press the "Log in" button
      And I am on "/user/1"
     Then I should see "Member for"
      And I should not see "Access denied" in the "content" region

  @javascript
  Scenario: Confirm that we can login manually works with javascript (via Selenium).
    Given I am not logged in
     When I am on "/user/1"
     Then I should see "Access denied"
      And I should not see "Member for"
    Given I am on "/user/login"
     When I enter "dev" for "Username"
      And I enter "pwd" for "Password"
      And I press the "Log in" button
      And I am on "/user/1"
     Then I should see "Member for"
      And I should not see "Access denied"
