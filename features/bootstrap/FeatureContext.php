<?php

use Behat\Mink\Exception\ExpectationException;
use Drupal\DrupalExtension\Context\RawDrupalContext;


/**
 * Defines application features from the specific context.
 */
class FeatureContext extends RawDrupalContext
{
    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct() {}

    /**
     * @Given I wait :seconds seconds
     */
    public function iWait($seconds)
    {
        sleep($seconds);
    }

}
