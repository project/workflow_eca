# Run our test suite.

.PHONY: tests tests-wip vnc test-steps

prune-test-cruft:
	ddev stop
	docker network prune
	docker image prune
	docker container prune
	docker volume prune

tests: ## Run functional Behat test suite
	$(make) snapshot
	$(BEHAT) --stop-on-failure

test-wip:
	$(BEHAT) --stop-on-failure --tags=wip
tests-wip: test-wip

test-steps:
	$(BEHAT) -dl

# @TODO: set an ENV variable for users *actual* vncviewer?
vnc: ## Spawn xvncviewer to see local chromedriver running.
	@$(ECHO) "$(ORANGE)Consider using the in-browser client: $(BLUE)https://SITE.ddev.site:7900/$(RESET)"
	@$(ECHO) "$(YELLOW)Beginning spawn of xvncviewer in background.$(RESET)"
	@xvncviewer localhost:5900
	@$(ECHO) "$(YELLOW)Completed spawn of xvncviewer in background.$(RESET)"
