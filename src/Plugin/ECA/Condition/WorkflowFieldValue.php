<?php

namespace Drupal\workflow_eca\Plugin\ECA\Condition;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\Context\ContextRepositoryInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\TypedData\ComplexDataInterface;
use Drupal\Core\TypedData\PrimitiveInterface;
use Drupal\Core\TypedData\TraversableTypedDataInterface;
use Drupal\eca\EcaState;
use Drupal\eca\Plugin\ECA\Condition\ConditionInterface;
use Drupal\eca\Plugin\ECA\Condition\ConditionBase;
use Drupal\eca\Token\TokenInterface;
use Drupal\eca\TypedData\PropertyPathTrait;
use Drupal\workflow\Entity\Workflow;
use Drupal\workflow\Entity\WorkflowState;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Plugin implementation of the ECA condition for workflow field value.
 *
 * @EcaCondition(
 *   id = "workflow_eca_field_value",
 *   label = @Translation("PMT: compare workflow field value"),
 *   description = @Translation("Compares workflow field value."),
 *   context_definitions = {
 *     "node" = @ContextDefinition("node_route_context:node", label = @Translation("Node"), required = FALSE)
 *   }
 * )
 */
class WorkflowFieldValue extends ConditionBase {

  use PropertyPathTrait;

  /**
   * {@inheritdoc}
   *
   * This class takes care of Token replacements on its own.
   */
  protected static bool $replaceTokens = FALSE;

  /**
   * The configured and tokenized field name.
   *
   * @var string|null
   */
  protected ?string $fieldName;

  /**
   * The configured value as expected field value.
   *
   * @var string[]|null
   */
  protected ?array $expectedValue;

  /**
   * The target field value to evaluate the expected value against.
   *
   * @var string|null
   */
  protected ?string $targetValue;

  /**
   * Context repository.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected ContextRepositoryInterface $contextRepository;

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    RequestStack $request_stack,
    TokenInterface $token_services,
    AccountProxyInterface $current_user,
    TimeInterface $time,
    EcaState $state,
    ContextRepositoryInterface $contextRepository,
  ) {
    $this->contextRepository = $contextRepository;
    parent::__construct($configuration, $plugin_id, $plugin_definition,
      $entity_type_manager,
      $entity_type_bundle_info,
      $request_stack,
      $token_services,
      $current_user,
      $time,
      $state,
    );

    // @todo Implement this: $this->>setDefinedContextValues();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): ConditionBase {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info'),
      $container->get('request_stack'),
      $container->get('eca.token_services'),
      $container->get('current_user'),
      $container->get('datetime.time'),
      $container->get('eca.state'),
      $container->get('context.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function evaluate(): bool {
    $target_value = $this->targetValue ?? $this->getTargetValue();
    $expected_value = $this->getExpectedValue();

    // There is a special case here. The '_creation' states are never actually
    // assigned to the field value, but show up as the workflow field having a
    // NULL value. We need to represent this in the expected value.
    if ($target_value == NULL) {
      $match = $this->isExpectedCreationState($expected_value);
    }
    else {
      $match = in_array($target_value, $expected_value);
    }

    return $this->negationCheck($match);
  }

  /**
   * {@inheritdoc}
   */
  public function reset(): ConditionInterface {
    $this->fieldName = NULL;
    $this->expectedValue = NULL;
    $this->targetValue = NULL;
    return parent::reset();
  }

  /**
   * Get the configured and tokenized field name.
   *
   * @return string
   *   The tokenized field name.
   */
  protected function getFieldName(): string {
    if (!isset($this->fieldName)) {
      $this->fieldName = trim((string) $this->tokenServices->replaceClear($this->configuration['field_name'] ?? ''));
    }
    return $this->fieldName;
  }

  /**
   * Get the configured and tokenized value as expected field value.
   *
   * @return string[]
   *   The expected value.
   */
  protected function getExpectedValue() {
    if (!isset($this->expectedValue)) {
      assert(is_array($this->configuration['expected_value']));
      $this->expectedValue = [];
      foreach ($this->configuration['expected_value'] as $key => $value) {
        $this->expectedValue[$key] = trim($this->tokenServices->replaceClear($value ?? ''));
      }
    }
    return $this->expectedValue;
  }

  /**
   * Check if a given state ID is a creation state.
   *
   * @return bool
   *   TRUE if the $sid is a creation workflow state.
   */
  protected function isExpectedCreationState($sids) {
    $match = FALSE;
    foreach ($sids as $sid) {
      $state = WorkflowState::load($sid);
      if ($state->isCreationState()) {
        $match = TRUE;
      }
    }
    return $match;
  }

  /**
   * Get the target field value to evaluate against.
   *
   * @return string|null
   *   The target field value as string or NULL if no value is present.
   */
  protected function getTargetValue(): ?string {
    $field_values = $this->getFieldValues();
    $num_items = count($field_values);
    switch ($num_items) {
      case 0:
        $target_value = NULL;
        break;

      case 1:
        $target_value = reset($field_values);
        break;

      default:
        // When the field contains multiple values, we evaluate against
        // every single item and stick with the first match found.
        $target_value = NULL;
        $negated = $this->isNegated();
        $this->configuration['negate'] = FALSE;
        foreach ($field_values as $field_value) {
          $this->targetValue = $field_value;
          if ($this->evaluate()) {
            $target_value = $field_value;
            break;
          }
        }
        $this->targetValue = NULL;
        $this->configuration['negate'] = $negated;
    }
    return $target_value;
  }

  /**
   * Get a list of field values to evaluate against.
   *
   * @return string[]
   *   The list of field values.
   */
  protected function getFieldValues(): array {
    $field_name = $this->getFieldName();
    $entity = $this->getEntity();
    $values = [];
    $options = [
      'auto_append' => FALSE,
      'auto_item' => FALSE,
      'access' => FALSE,
    ];

    // Special case: if $entity->isNew(), then the Workflow field value can vary depending on the form_state.
    // We should treat this as the Creation state regardless of the Workflow field, thus return an empty array.
    if ($entity && $entity->isNew()) {
      return $values;
    }


    if ($entity && ($list = $this->getTypedProperty($entity->getTypedData(), $field_name, $options))) {
      if (!($list instanceof TraversableTypedDataInterface)) {
        $list = [$list];
      }
      /** @var \Drupal\Core\TypedData\TypedDataInterface $property */
      foreach ($list as $property) {
        if ($property instanceof ComplexDataInterface) {
          $main_property = $property->getDataDefinition()->getMainPropertyName();
          if ($main_property !== NULL) {
            $property = $property->get($main_property);
          }
        }
        if ($property instanceof PrimitiveInterface) {
          $values[] = (string) $property->getValue();
        }
      }
    }
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration(): array {
    return [
      'expected_workflow' => '',
      'expected_value' => '',
      'field_name' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    $form['field_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Workflow field name'),
      '#default_value' => $this->configuration['field_name'] ?? '',
      '#description' => $this->t('The machine name of the workflow field whose value to compare.'),
      '#weight' => -90,
    ];
    /** @var \Drupal\workflow\Entity\Workflow $workflows */
    $workflows = Workflow::loadMultiple();
    $workflow_options = [];
    $state_options = [];
    foreach ($workflows as $wid => $workflow) {
      $workflow_options[$wid] = $workflow->label();

      if ($triggering_element = $form_state->getTriggeringElement()) {
        $expected_workflow = $triggering_element['#value'];
      }
      else {
        $expected_workflow = $this->configuration['expected_workflow'] ?? '';
      }

      if (isset($expected_workflow)) {
        if ($wid == $expected_workflow) {
          $state_options = $this->buildWorkflowStateOptions($wid);
        }
      }
      else {
        /** @var \Drupal\workflow\Entity\WorkflowState[] $states */
        /** @var \Drupal\workflow\Entity\WorkflowState $state */
        $states = WorkflowState::loadMultiple(NULL, $wid);
        foreach ($states as $sid => $state) {
          $state_options[$sid] = $workflow->label . ': ' . $state->label();
        }
      }
    }

    $form['expected_workflow'] = [
      '#type' => 'select',
      '#title' => $this->t('Workflow'),
      '#description' => $this->t('Workflow in which expected State exists.'),
      '#default_value' => $this->configuration['expected_workflow'] ?? '',
      '#options' => $workflow_options,
      '#weight' => -80,
      '#multiple' => FALSE,
      '#ajax' => [
        'callback' => [$this, 'workflowStatesCallback'],
        'event' => 'change',
        'wrapper' => 'expected-value',
        'progress' => [
          'type' => 'throbber',
          'message' => 'Loading states...',
        ],
      ],
    ];
    $form['expected_value'] = [
      '#type' => 'select',
      '#title' => $this->t('Expected State'),
      '#description' => $this->t('The Workflow State in which the field is expected to be.'),
      '#default_value' => $this->configuration['expected_value'] ?? '',
      '#options' => $state_options,
      '#weight' => -70,
      '#multiple' => TRUE,
      '#prefix' => '<div id="expected-value">',
      '#suffix' => '</div>',
    ];
    return parent::buildConfigurationForm($form, $form_state);
  }

  /**
   * Ajax callback for workflow states of a given workflow.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return mixed
   *   The new expected_value form array.
   */
  public function workflowStatesCallback(array &$form, FormStateInterface $form_state) {
    return $form['condition']['configuration']['expected_value'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->configuration['field_name'] = $form_state->getValue('field_name');
    $this->configuration['expected_workflow'] = $form_state->getValue('expected_workflow');
    $this->configuration['expected_value'] = $form_state->getValue('expected_value');
    parent::submitConfigurationForm($form, $form_state);
  }

  /**
   * Get the entity to act upon.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The entity, or NULL if not found.
   */
  public function getEntity(): ?EntityInterface {
    // When we are evaluating this condition in the context of an entity
    // referenced by the node whose workflow value is in question, The standard
    // "node" context provider returns the referenced entity, rather than the
    // "parent". We therefore look up the relevant node by the
    // node_route_context provider explicitly. There is likely a better way!
    if ($node = $this->getValueFromContext('entity')) {
      return $node;
    }
    $node_context = '@node.node_route_context:node';
    $context_mapping = ['node' => $node_context];
    $contexts = $this->contextRepository->getRuntimeContexts($context_mapping);
    return $contexts[$node_context]->getContextValue('node');
  }

  /**
   * Gather a set of workflow state options (with labels) for a given workflow.
   *
   * @param int $wid
   *   The workflow ID.
   *
   * @return array
   *   The set of state labels, keyed by state id.
   */
  protected function buildWorkflowStateOptions($wid) {
    $options = [];
    /** @var \Drupal\workflow\Entity\WorkflowState[] $states */
    /** @var \Drupal\workflow\Entity\WorkflowState $state */
    $states = WorkflowState::loadMultiple(NULL, $wid);
    foreach ($states as $sid => $state) {
      $options[$sid] = $state->label();
    }
    return $options;
  }

}
